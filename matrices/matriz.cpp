#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#define filas 3 
#define columnas 3

int i,j,k; 
int matriz [filas][columnas];
int matriz2 [filas][columnas];
int resultado [filas][columnas];
	int
main(int argc, char *argv[])
{
	//Introduccion de matrices
	printf ("Introduce la primera matriz: \n");
	for (i=0; i<filas; i++)
		for( j=0; j<columnas; j++)
			scanf("%i", &matriz[i][j]);
	printf("\n");
	//Introduccion de la segunda matriz
	printf ("Introduce la segunda matriz: \n");
	for (i=0; i<filas; i++)
		for( j=0; j<columnas; j++)
			scanf("%i", &matriz2[i][j]);
	printf("\n");
	//Multiplicacion de matrices 
	bzero(resultado, sizeof(resultado));
	for (i=0; i<filas; i++){
		for(j=0; j<columnas; j++)
		{
			resultado [i][j]=0;
			for(k=0; k<columnas; k++)
				resultado [i][j]+=matriz[i][k]*matriz2[k][j];
		}
	}
//Resultdo de la multiplicacion 
	printf("Resultado de la mattriz: \n ");
	for (i=0; i<filas; i++){
		printf("\n");
		for( j=0; j<columnas; j++)
			printf("%7i", resultado[i][j]);
	}
	printf("\n");
	return EXIT_SUCCESS;
}
