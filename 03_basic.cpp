#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

#define MAX 0X20
   int
   main () {
        unsigned rep=0;
        char nombre[MAX];
        unsigned dia,
                 mes,
                 anio;
    
       do {

       printf("Repeticiones: ");
          __fpurge (stdin);
    } while (!scanf("%u", &rep) ) ;

       printf("Cual es tu nombre:");
       scanf("%s", nombre);


      for(unsigned i=0; i<rep; i++)
          printf("te llamas %s \n ", nombre);
      
      printf ("¿Cuál es tu fecha de nacimiento?:");
      scanf ("%u %*1[-/] %u %*1[-/] %u", &dia, &mes, &anio);


      printf ("%u,%u,%u\n", dia, mes, anio);


      return EXIT_SUCCESS;
  }

