#include <stdio.h>
#include <stdlib.h>
void 
preguntar (unsigned *age, char nombre []){
	printf ("Edad: ");
	scanf ("%u", age);
	printf ("Nombre: ");
	scanf("%s", nombre);
	
	*age += 2;
}
int 
main (int argc, char *argv[])
{
	unsigned edad; 
	char nombre [];

	preguntar (&edad, nombre);
	
	return EXIT_SUCCESS;
}
