#include <stdio.h>
#include <stdlib.h> 

void 
error() {
    fprintf (stderr, "Has metido un numero mal. \n");
    exit(EXIT_FAILURE);
}

void 
ver_media (double media){
    printf("===========\n"
            " Media: .2lf\n"
            "===========\n"
            , media);
    printf("\n");
}
int 
main
