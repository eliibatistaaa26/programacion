//ESto es un comentario de una linea
/* Ccomentario de multiples lineas */
int a;  //Variable global.
        //Declarada fuera de todas las funciones.
        //Se puede usar desde esta linea en adelante.
        //No se deben usar de manera habitual.
        //VG por defecto =0

void        //No devuelve nada : void = vacio
haz_algo    //Usamos la notacion underscore 
           //En camelcase : hazAlgo (no usar)
{
    return;   //Cuando se ejecuta vuele al lugar 
              //Desde donde le he llamado 
              //Puede deolver un valor o no
              //Es innecesario en las funciones void 
              //Caundo una f void llega al final
              //Ejecuta un return por si sola
}

int       // Main devuelve un entero
main()    //declarando la funcion main, quiere decir que existe un nombre nuevo
{         /*conjunto de instrucciones */
  int c;    // Variables local.
            // Se define dentro de {}
            // Solo se conoce en la función.
            // Al terminar la funcion su valor desaparece.
          // POr defecto ? (rubbish)

    2+a;  // LAs instrucciones terminan en ;
          // Esto es una expresion.
          // Todo tiene un valor de retorno, pero...
          // ¿Ques estamos haciendo con el VR?
          
    return 0;   //Devuelve un valor a la funcion que llamó.
                //main => bash
                //0=> todo ok
                //!= 0 => wrong
}
