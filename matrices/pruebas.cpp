
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define FILAS [f] 
#define COLUMNAS [c]

void multiplicacion (int resultado[FILAS][COLUMNAS], int matriz[FILAS][COLUMNAS], int matriz2[FILAS][COLUMNAS] )
{

    bzero(resultado, sizeof(resultado));
    for (int i=0; i<FILAS; i++)
	for(int j=0; j<COLUMNAS; j++) {
	    resultado[i][j] = 0;
	    for(int k=0; k<COLUMNAS; k++)
		resultado[i][j] += matriz[i][k] * matriz2[k][j];
	}
}

int
main (int argc, char *argv[])
{
    //Declaracion de variables 
    int matriz [FILAS][COLUMNAS];
    int matriz2 [FILAS][COLUMNAS];
    int resultado [FILAS][COLUMNAS];

    //Introduccion de matrices
    printf ("Introduce la primera matriz: \n");
    for (int i=0; i<FILAS; i++)
	for( int j=0; j<COLUMNAS; j++)
	    scanf("%i", &matriz[i][j]);
    printf("\n");

    //Introduccion de la segunda matriz
    printf ("Introduce la segunda matriz: \n");
    for (int i=0; i<FILAS; i++)
	for( int j=0; j<COLUMNAS; j++)
	    scanf("%i", &matriz2[i][j]);
    printf("\n");

    //Multiplicacion de matrices 
    multiplicacion (resultado, matriz, matriz2);
    
    //Resultdo de la multiplicacion 
    printf("Resultado de la mattriz: \n ");
    for (int i=0; i<FILAS; i++){
	printf("\n");
	for( int j=0; j<COLUMNAS; j++)
	    printf("%7i", resultado[i][j]);
    }
    printf("\n");
    return EXIT_SUCCESS;
}
